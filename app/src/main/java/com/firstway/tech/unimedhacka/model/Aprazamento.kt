package com.firstway.tech.unimedhacka.model

import com.google.gson.annotations.SerializedName

data class Aprazamento(
        @SerializedName("dataUltimoAtendimentoFormatada")
        val dataUltimoAtendimentoFormatada: String
)
