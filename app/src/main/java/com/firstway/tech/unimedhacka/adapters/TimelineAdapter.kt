package com.firstway.tech.unimedhacka.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.firstway.tech.unimedhacka.R
import com.firstway.tech.unimedhacka.models.TimelineModel
import com.firstway.tech.unimedhacka.utils.TipoHistorico
import com.firstway.tech.unimedhacka.utils.VectorDrawableUtils
import com.github.vipulasri.timelineview.TimelineView

class TimelineAdapter(
        private val mFeedList: List<TimelineModel>,
        private val context: Context) : RecyclerView.Adapter<TimelineAdapter.TimeLineViewHolder>() {

    var onItemClick: ((TimelineModel) -> Unit)? = null

    override fun getItemViewType(position: Int): Int {
        return TimelineView.getTimeLineViewType(position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeLineViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.item_timeline, parent, false)
        return TimeLineViewHolder(view)
    }

    override fun onBindViewHolder(holder: TimeLineViewHolder, position: Int) {

        val timeLineModel = mFeedList[position]
        holder.date.text = timeLineModel.date
        holder.message.text = timeLineModel.description

        when (position) {
            0 -> {
                holder.timeline.initLine(1)
            }
            (mFeedList.size - 1) -> {
                holder.timeline.initLine(2)
            }
            else -> {
                holder.timeline.initLine(0)
            }
        }

        when(timeLineModel.tipoHistorico) {
            TipoHistorico.EXAME -> setMarker(holder, R.drawable.ic_flask, R.color.md_purple_500)
            TipoHistorico.CONSULTA -> setMarker(holder, R.drawable.ic_account_search, R.color.md_blue_grey_500)
            TipoHistorico.SINTOMAS -> setMarker(holder, R.drawable.ic_account_question, R.color.md_blue_400)
            TipoHistorico.OUTROS -> setMarker(holder, R.drawable.ic_account_arrow_down, R.color.md_orange_800)
            TipoHistorico.INTERNACAO -> setMarker(holder, R.drawable.ic_bed, R.color.md_deep_orange_A400)
        }

        if(timeLineModel.tipoHistorico == TipoHistorico.EXAME) {
            holder.image.visibility = VISIBLE
        } else {
            holder.image.visibility = GONE
        }
    }

    private fun setMarker(holder: TimeLineViewHolder, drawableResId: Int, colorFilter: Int) {
        holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.context, colorFilter))
    }

    override fun getItemCount() = mFeedList.size

    inner class TimeLineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val date = itemView.findViewById<AppCompatTextView>(R.id.text_timeline_date)
        val message = itemView.findViewById<AppCompatTextView>(R.id.text_timeline_title)
        val timeline = itemView.findViewById<TimelineView>(R.id.timeline)
        val image = itemView.findViewById<ImageView>(R.id.image_document)

        init {
            timeline.initLine(2)
            itemView.setOnClickListener {
                onItemClick?.invoke(mFeedList[adapterPosition])
            }
        }
    }
}