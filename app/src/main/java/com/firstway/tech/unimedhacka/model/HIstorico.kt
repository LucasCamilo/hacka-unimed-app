package com.firstway.tech.unimedhacka.model

import com.google.gson.annotations.SerializedName

data class Historico(
        @SerializedName("tipo")
        val tipo: String,
        @SerializedName("titulo")
        val titulo: String,
        @SerializedName("aprazamento")
        val aprazamento: Aprazamento
)