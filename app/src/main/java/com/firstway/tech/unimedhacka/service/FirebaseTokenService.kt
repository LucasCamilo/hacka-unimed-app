package com.firstway.tech.unimedhacka.service

import com.firstway.tech.unimedhacka.model.TokenFirebaseModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface FirebaseTokenService {

    @Headers("Content-Type: application/json")
    @POST("firebase_token")
    fun addFirebaseToken(@Body token: TokenFirebaseModel): Call<String>
}