package com.firstway.tech.unimedhacka.models

import com.firstway.tech.unimedhacka.utils.TipoHistorico

data class TimelineModel(
        val description: String,
        val date: String,
        val tipoHistorico: TipoHistorico)