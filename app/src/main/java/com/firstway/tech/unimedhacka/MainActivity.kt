package com.firstway.tech.unimedhacka

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.firstway.tech.unimedhacka.activity.InputActivity
import com.firstway.tech.unimedhacka.activity.OnboardingActivity
import com.firstway.tech.unimedhacka.dialog.CustomDialog
import com.firstway.tech.unimedhacka.fragments.TimelineFragment
import com.firstway.tech.unimedhacka.model.TokenFirebaseModel
import com.firstway.tech.unimedhacka.retrofit.RetrofitInitializer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private lateinit var fab: View

    companion object {
        private var TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, TimelineFragment.newInstance())
                    .commitNow()
        }

        val intent = Intent(applicationContext, OnboardingActivity::class.java)
        startActivity(intent)
        getFirebaseToken()
        configureFabIcon()
    }

    private fun getFirebaseToken() {
        Firebase.messaging.token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            if (token != null) {
                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d(TAG, msg)

                // SAVE FCM KEY
                var call = RetrofitInitializer().FirebaseTokenService().addFirebaseToken(
                    TokenFirebaseModel(token)
                )
                call.enqueue(object: Callback<String> {
                    override fun onResponse(call: Call<String>?,
                                            response: Response<String>?) {
                        Log.d(TAG, "FCM registrada com sucesso ${response?.body()}")
                    }

                    override fun onFailure(call: Call<String>?,
                                           t: Throwable?) {
                        Log.e(TAG, "Falha ao registrar FCM")
                    }
                })
            }
        })
    }

    private fun configureFabIcon() {
        fab = findViewById(R.id.fab)
        fab.setOnClickListener {
            val i = Intent(this, InputActivity::class.java)
            startActivity(i)
        }
    }
}