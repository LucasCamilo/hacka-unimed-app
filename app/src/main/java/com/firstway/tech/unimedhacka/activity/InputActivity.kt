package com.firstway.tech.unimedhacka.activity

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.firstway.tech.unimedhacka.R
import com.firstway.tech.unimedhacka.dialog.CustomDialog

class InputActivity : AppCompatActivity() {
    private lateinit var btnSave: Button
    private lateinit var editTextTextMultiLine: EditText
    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input)

        preferences = this.getSharedPreferences("pref", Context.MODE_PRIVATE)

        editTextTextMultiLine = findViewById(R.id.editTextTextMultiLine)
        btnSave = findViewById(R.id.button)
        btnSave.setOnClickListener {

            if(editTextTextMultiLine.text.isNullOrEmpty()) {
                Toast.makeText(this, "Informe a descrição!", Toast.LENGTH_LONG).show()
            } else {
                val customDialog = CustomDialog()
                customDialog.show(supportFragmentManager, "InputActivity")

                with(preferences.edit()) {
                    putString("TEXT", editTextTextMultiLine.text.toString())
                    apply()
                }

                Handler().postDelayed(Runnable {
                    customDialog.dismiss()
                    finish()
                }, 1000)
            }
        }
    }
}