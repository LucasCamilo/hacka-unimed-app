package com.firstway.tech.unimedhacka.model

import com.google.gson.annotations.SerializedName

data class TokenFirebaseModel (
    @SerializedName("token")
    val token: String
)