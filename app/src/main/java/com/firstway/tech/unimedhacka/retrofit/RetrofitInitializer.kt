package com.firstway.tech.unimedhacka.retrofit

import com.firstway.tech.unimedhacka.service.BeneficiarioService
import com.firstway.tech.unimedhacka.service.FirebaseTokenService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://rhv41rzd8d.execute-api.us-east-1.amazonaws.com/Prod/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun FirebaseTokenService(): FirebaseTokenService = retrofit.create(FirebaseTokenService::class.java)

    fun BeneficiarioService(): BeneficiarioService = retrofit.create(BeneficiarioService::class.java)
}