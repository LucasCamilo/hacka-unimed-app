package com.firstway.tech.unimedhacka.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.firstway.tech.unimedhacka.R
import com.firstway.tech.unimedhacka.dialog.CustomDialog
import com.firstway.tech.unimedhacka.dialog.SuccessDialog


class ResultActivity : AppCompatActivity() {

    private lateinit var btnOpenDocument: Button
    private lateinit var btnOptout: Button
    private lateinit var btnOptin: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        btnOpenDocument = findViewById(R.id.btnOpenDocument)
        btnOpenDocument.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://hacka-unimed.s3.us-east-1.amazonaws.com/56105C80-8ECB-4237-9C5F-20BA3F6A7D16.pdf?response-content-disposition=inline&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEIr%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXNhLWVhc3QtMSJHMEUCICf4%2FUer1NV0vZXzhZvqmcUadDbseVFdk4lhI%2F0NI2HnAiEAxQAhqqQzPg3S3VkqO6sV2OMHPCO4kDCW8F2hFfsxyL4q7QIIw%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARABGgwxMzY3ODIzNzcwMjQiDIhAfLH1FCHmHopudirBAtrRPgbc2wgsSVVbVmGEJkqad1cVdL0jZ77gyq5Mu8YzA9OEptlpP%2FsUnjI96W1PxBxXaWjmOeUJXjUJXOKeab5%2B5u2m0qNE7O6Lese8HwPu1d2cj4j7bwCHbZ764ZU3cMLxsK4Y%2FSpk%2FIm%2BdfF4oA8FfpHsOdKup%2B07dW4WEgRW852ZgWxi7xoE2EwBwOH%2B5uB7rjlbh5kcDcSQ14WHFvlwC85t7JFC6kGvrlUu2g7IkiOt%2F%2BQIKtgJdEZcKiumOG4SF1640K97Z5hMHQhTkIl6ILWVVXwaSor%2BDNdYfdIxuBhYWhdCyJzrBlp%2Fy2GcU31rApJoj8qNKoE2eibjoZF31U%2Fwu88F4v2wcj3MZsGXszVYEhfQVG6qzPGWNsJZEO8OyQamJcBWMcEG6FHalxhdLd4DNPV0uZrd79hszW4l7TCVnICQBjqzAkUXw0hJq0GAJMUnLWbAILnAXcoQ0WsPdlO%2FYBDlBNTERfqXsNHDvHicLvBWXneUMXTYjG0YmVCKE2dgLd5ft%2FXXcWvTxFhYtNcJqKGcchc3Z7epBi0Eg5LrBlht5coTHFXFU%2FQDBOU7qQEZ%2FnC%2FEHJpBHKNXwZSHHIBBdzGQiE2K45X%2FmXrJrdtIeHQ%2B9lpAl4zkADCNCoNzbWMWN75NOFTPVSu7AfLGsh27ppwMrr50ieXqr1g7qrmGdwVbBhmixW6QfJWmVXnqCtc2EbP4W2l7ECOoLzY8%2Bm8Cvv7gCRs%2BFoZvDWGCZQ6z26hrgma0Y4Rjd4%2FaSE3ukdCy0hyNWxuR4GshvLTPmfV54C4L2wfW3VAiHiQS1dDbT9ch0wKXOF%2F9cH4XXR0z%2BbKdx0y1yPhQMw%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220206T182434Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAR7WG5OBADDFABC4X%2F20220206%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=025f40ab58ced0bec903ae2b920b490ca9d894487fbe1219d890ec868e58e715"))
            startActivity(browserIntent)
        }

        btnOptout = findViewById(R.id.btnOptout)
        btnOptout.setOnClickListener {
            //TODO: SHOW MODAL WITH MESSAGE
        }

        btnOptin = findViewById(R.id.btnOptin)
        btnOptin.setOnClickListener {
            //TODO: SEND TO BACKEND OPTIN FOR DOCUMENT
            //TODO: SHOW SCREEN SUCESS
            val successDialog = SuccessDialog()
            successDialog.show(supportFragmentManager, "ResultActivity")

            Handler().postDelayed(Runnable {
                successDialog.dismiss()
                finish()
            }, 2000)
        }
    }
}