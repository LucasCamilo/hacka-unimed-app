package com.firstway.tech.unimedhacka.service

import com.firstway.tech.unimedhacka.model.Beneficiario
import retrofit2.Call
import retrofit2.http.GET

interface BeneficiarioService {
    @GET("beneficiario/57fa28ae-d443-4f34-9f78-156e21f166ed")
    fun getBeneficiario(): Call<Beneficiario>
}