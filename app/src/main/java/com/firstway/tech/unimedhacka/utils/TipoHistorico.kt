package com.firstway.tech.unimedhacka.utils

enum class TipoHistorico {
    EXAME,
    CONSULTA,
    INTERNACAO,
    SINTOMAS,
    OUTROS
}