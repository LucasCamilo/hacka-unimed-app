package com.firstway.tech.unimedhacka.model

import com.google.gson.annotations.SerializedName

data class Beneficiario (
        @SerializedName("nomeCompleto")
        val nomeCompleto: String,
        @SerializedName("historico")
        val historico: MutableList<Historico>
        )